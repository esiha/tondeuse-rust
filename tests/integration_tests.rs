use std::fs::File;
use fluent_asserter::prelude::*;
use tondeuse_rust::position::{CardinalPoint, Point, Position};
use tondeuse_rust::run_mowers;

#[test]
fn should_run_known_configuration() {
    let positions = run_mowers(File::open("tests/input").expect("failed to open file"));

    assert_that!(positions.is_ok()).is_true();
    assert_that!(positions.unwrap()).is_equal_to(vec![
        Position {
            at: Point { x: 1, y: 3 },
            heading: CardinalPoint::North,
        },
        Position {
            at: Point { x: 4, y: 1 },
            heading: CardinalPoint::East,
        },
    ])
}