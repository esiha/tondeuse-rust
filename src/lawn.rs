use crate::position::Point;

pub trait Lawn {
    fn contains(&self, point: &Point) -> bool;
}

struct RectangularLawn {
    bottom_left_corner: Point,
    top_right_corner: Point,
}

impl Lawn for RectangularLawn {
    fn contains(&self, point: &Point) -> bool {
        point.is_in_top_right_quadrant_of(&self.bottom_left_corner)
            && point.is_in_bottom_left_quadrant_of(&self.top_right_corner)
    }
}

pub fn rectangular(top_right_corner: Point) -> impl Lawn {
    RectangularLawn {
        bottom_left_corner: Point { x: 0, y: 0 },
        top_right_corner,
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use rstest::*;
    use fluent_asserter::prelude::*;

    mod rectangular_lawn {
        use super::*;

        #[rstest]
        #[case(Point {x: 5, y: 5}, true)]
        #[case(Point {x: 3, y: 3}, true)]
        #[case(Point {x: 0, y: 0}, true)]
        #[case(Point {x: 6, y: 0}, false)]
        #[case(Point {x: 0, y: - 1}, false)]
        fn contains(#[case] input: Point, #[case] expected: bool) {
            let lawn = rectangular(Point { x: 5, y: 5 });

            assert_that!(lawn.contains(&input)).is_equal_to(expected);
        }
    }
}