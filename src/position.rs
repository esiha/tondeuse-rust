use std::ops::Add;
use crate::position::CardinalPoint::*;

#[derive(PartialEq, Copy, Clone, Debug)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}

impl Add for Point {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Point {
    pub fn is_in_bottom_left_quadrant_of(&self, other: &Point) -> bool {
        self.x <= other.x && self.y <= other.y
    }

    pub fn is_in_top_right_quadrant_of(&self, other: &Point) -> bool {
        self.x >= other.x && self.y >= other.y
    }
}

#[derive(PartialEq, Copy, Clone, Debug)]
pub enum CardinalPoint {
    North,
    East,
    South,
    West,
}

impl CardinalPoint {
    fn rotate_90_degrees_left(self) -> CardinalPoint {
        match self {
            North => West,
            West => South,
            South => East,
            East => North
        }
    }

    fn rotate_90_degrees_right(self) -> CardinalPoint {
        match self {
            North => East,
            East => South,
            South => West,
            West => North
        }
    }

    fn as_vector(self) -> Point {
        match self {
            North => Point { x: 0, y: 1 },
            East => Point { x: 1, y: 0 },
            South => Point { x: 0, y: -1 },
            West => Point { x: -1, y: 0 }
        }
    }
}

#[derive(PartialEq, Copy, Clone, Debug)]
pub struct Position {
    pub at: Point,
    pub heading: CardinalPoint,
}

impl Position {
    pub fn rotate_90_degrees_left(self) -> Position {
        Position { at: self.at, heading: self.heading.rotate_90_degrees_left() }
    }

    pub fn rotate_90_degrees_right(self) -> Position {
        Position { at: self.at, heading: self.heading.rotate_90_degrees_right() }
    }

    pub fn translate_once(self) -> Position {
        Position { at: self.at + self.heading.as_vector(), heading: self.heading }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use rstest::*;
    use fluent_asserter::prelude::*;

    mod point {
        use super::*;

        #[test]
        fn add_adds_x_and_y() {
            assert_that!(Point { x: 5, y: 4 } + Point { x: 1, y: 3 })
                .is_equal_to(Point { x: 6, y: 7 });
        }

        #[rstest]
        #[case(Point {x: 5, y: 5}, true)]
        #[case(Point {x: 4, y: 5}, true)]
        #[case(Point {x: 5, y: 4}, true)]
        #[case(Point {x: 4, y: 4}, true)]
        #[case(Point {x: 6, y: 5}, false)]
        #[case(Point {x: 5, y: 6}, false)]
        #[case(Point {x: 6, y: 6}, false)]
        fn is_in_bottom_left_quadrant_of(#[case] input: Point, #[case] expected: bool) {
            let reference = Point { x: 5, y: 5 };
            assert_that!(input.is_in_bottom_left_quadrant_of(&reference)).is_equal_to(expected);
        }

        #[rstest]
        #[case(Point {x: 5, y: 5}, true)]
        #[case(Point {x: 6, y: 5}, true)]
        #[case(Point {x: 5, y: 6}, true)]
        #[case(Point {x: 6, y: 6}, true)]
        #[case(Point {x: 4, y: 5}, false)]
        #[case(Point {x: 5, y: 4}, false)]
        #[case(Point {x: 4, y: 4}, false)]
        fn is_in_top_right_quadrant_of(#[case] input: Point, #[case] expected: bool) {
            let reference = Point { x: 5, y: 5 };
            assert_that!(input.is_in_top_right_quadrant_of(&reference)).is_equal_to(expected);
        }
    }

    mod cardinal_point {
        use super::*;

        #[rstest]
        #[case(North, West)]
        #[case(West, South)]
        #[case(South, East)]
        #[case(East, North)]
        fn rotate_90_degrees_left(#[case] input: CardinalPoint, #[case] expected: CardinalPoint) {
            assert_that!(input.rotate_90_degrees_left()).is_equal_to(expected);
        }

        #[rstest]
        #[case(North, East)]
        #[case(East, South)]
        #[case(South, West)]
        #[case(West, North)]
        fn rotate_90_degrees_right(#[case] input: CardinalPoint, #[case] expected: CardinalPoint) {
            assert_that!(input.rotate_90_degrees_right()).is_equal_to(expected);
        }

        #[rstest]
        #[case(North, Point {x: 0, y: 1})]
        #[case(East, Point {x: 1, y: 0})]
        #[case(South, Point {x: 0, y: - 1})]
        #[case(West, Point {x: - 1, y: 0})]
        fn as_vector(#[case] input: CardinalPoint, #[case] expected: Point) {
            assert_that!(input.as_vector()).is_equal_to(expected);
        }
    }

    mod position {
        use super::*;

        #[test]
        fn rotate_90_degrees_left() {
            let at = Point { x: 4, y: 3 };
            let heading = North;

            assert_that!(Position {at, heading}.rotate_90_degrees_left())
                .is_equal_to(Position { at, heading: West })
        }

        #[test]
        fn rotate_90_degrees_right() {
            let at = Point { x: 4, y: 3 };
            let heading = West;

            assert_that!(Position {at, heading}.rotate_90_degrees_right())
                .is_equal_to(Position { at, heading: North })
        }

        #[test]
        fn translate_once() {
            let at = Point { x: 3, y: 4 };
            let heading = East;

            assert_that!(Position {at, heading}.translate_once())
                .is_equal_to(Position { at: Point { x: 4, y: 4 }, heading });
        }
    }
}