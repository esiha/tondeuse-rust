use std::fmt::{Display, Formatter};
use std::io::{BufReader, Read};

use crate::configuration::{ParseError, read_configuration};
use crate::mower::Mower;
use crate::position::{CardinalPoint, Position};
use crate::position::CardinalPoint::*;

pub mod position;
mod lawn;
mod mower;
mod configuration;

pub fn run_mowers(input: impl Read) -> Result<Vec<Position>, ParseError> {
    let configuration = read_configuration(BufReader::new(input))?;

    let lawn = lawn::rectangular(configuration.top_right_corner);

    Ok(configuration.mowers.into_iter()
        .map(|mc| Mower {
            current_position: mc.start_position,
            instructions: mc.instructions,
        })
        .map(|mut m| {
            m.run_on(&lawn);
            m.current_position
        })
        .collect()
    )
}

impl Display for Position {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {} {}", self.at.x, self.at.y, self.heading)
    }
}

impl Display for CardinalPoint {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            North => "N",
            East => "E",
            West => "W",
            South => "S",
        })
    }
}
