use Instruction::*;

use crate::lawn::Lawn;
use crate::position::Position;

#[derive(PartialEq, Copy, Clone, Debug)]
pub enum Instruction {
    MoveOnce,
    TurnLeft,
    TurnRight,
}

impl Instruction {
    fn execute(&self, to: &Position) -> Position {
        match self {
            MoveOnce => to.translate_once(),
            TurnLeft => to.rotate_90_degrees_left(),
            TurnRight => to.rotate_90_degrees_right()
        }
    }
}

#[derive(Debug)]
pub struct Mower {
    pub current_position: Position,
    pub instructions: Vec<Instruction>,
}

impl Mower {
    pub fn run_on(&mut self, lawn: &impl Lawn) {
        for &instruction in &self.instructions {
            let new_position = instruction.execute(&self.current_position);
            if lawn.contains(&new_position.at) {
                self.current_position = new_position;
            }
        }
    }
}

#[cfg(test)]
mod test {
    use fluent_asserter::prelude::*;

    use crate::lawn::rectangular;
    use crate::position::{CardinalPoint, Point};
    use crate::position::CardinalPoint::East;

    use super::*;

    #[test]
    fn should_remain_inside_lawn_when_moving() {
        let mut mower = Mower {
            current_position: Position { at: Point { x: 1, y: 2 }, heading: CardinalPoint::South },
            instructions: vec![TurnRight, MoveOnce, MoveOnce, TurnLeft, TurnLeft],
        };

        mower.run_on(&rectangular(Point { x: 3, y: 3 }));

        assert_that!(mower.current_position)
            .is_equal_to(Position { at: Point { x: 0, y: 2 }, heading: East });
    }
}