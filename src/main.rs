use std::env::args;
use std::fs::File;
use std::process::exit;
use tondeuse_rust::*;


fn main() {
    let args = args().collect::<Vec<String>>();
    if args.len() != 2 {
        println!("Usage: {} path/to/configuration", args[0]);
        exit(1);
    }

    let configuration_file = File::open(&args[1])
        .expect("failed to open file");

    run_mowers(configuration_file)
        .expect("failed to parse configuration")
        .into_iter()
        .for_each(|p| println!("{}", p));
}
