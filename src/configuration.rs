use std::io::{BufRead, Error, ErrorKind, Lines};
use std::num::ParseIntError;
use std::str::FromStr;

use crate::mower::Instruction;
use crate::position::{CardinalPoint, Point, Position};

#[derive(PartialEq, Debug)]
pub struct Configuration {
    pub top_right_corner: Point,
    pub mowers: Vec<MowerConfiguration>,
}

#[derive(PartialEq, Debug)]
pub struct MowerConfiguration {
    pub start_position: Position,
    pub instructions: Vec<Instruction>,
}

#[derive(PartialEq, Debug)]
pub struct ParseError {
    message: String,
}

pub fn read_configuration(input: impl BufRead) -> Result<Configuration, ParseError> {
    let mut lines = input.lines();
    Ok(Configuration {
        top_right_corner: Point::from_str(&first_line(&mut lines)?)?,
        mowers: mowers(lines)?,
    })
}

fn first_line(lines: &mut Lines<impl BufRead>) -> Result<String, ParseError> {
    lines.next()
        .map_or(
            Err(ParseError::new(String::from("empty configuration"))),
            |r| r.map_err(ParseError::from),
        )
}

fn mowers(lines: Lines<impl BufRead>) -> Result<Vec<MowerConfiguration>, ParseError> {
    let mut mowers = vec![];
    for r in pairs(lines) {
        match r {
            Ok((first, second)) => mowers.push(mower(first, second)?),
            Err(e) => return Err(ParseError::from(e))
        }
    }
    Ok(mowers)
}

fn mower(position: String, instructions: String) -> Result<MowerConfiguration, ParseError> {
    Ok(MowerConfiguration {
        start_position: position.parse()?,
        instructions: parse(instructions)?,
    })
}

fn parse(instructions: String) -> Result<Vec<Instruction>, ParseError> {
    let mut is = vec![];
    for i in instructions.chars() {
        match i.to_string().parse::<Instruction>() {
            Ok(i) => is.push(i),
            Err(e) => return Err(e)
        }
    }
    Ok(is)
}

fn pairs<B: BufRead>(lines: Lines<B>) -> Pairs<B> {
    Pairs { lines }
}

struct Pairs<B: BufRead> {
    lines: Lines<B>,
}

impl<B: BufRead> Iterator for Pairs<B> {
    type Item = Result<(String, String), Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let first = match self.lines.next() {
            Some(Err(e)) => return Some(Err(e)),
            o => o
        }?.unwrap();

        let second = match self.lines.next() {
            None => return Some(Err(Error::new(ErrorKind::UnexpectedEof, format!("incomplete Mower configuration: {}", first)))),
            Some(Err(e)) => return Some(Err(e)),
            o => o
        }.unwrap().unwrap();

        Some(Ok((first, second)))
    }
}

impl ParseError {
    fn new(message: String) -> ParseError {
        ParseError { message }
    }
}

impl From<Error> for ParseError {
    fn from(e: Error) -> Self {
        ParseError::new(e.to_string())
    }
}

impl From<ParseIntError> for ParseError {
    fn from(e: ParseIntError) -> Self {
        ParseError::new(e.to_string())
    }
}

impl FromStr for Point {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts = s.split_whitespace().collect::<Vec<&str>>();
        if parts.len() != 2 {
            Err(ParseError::new(format!("expected 2 parts for a Point, got {}", parts.len())))
        } else {
            Ok(Point::from(parts[0], parts[1])?)
        }
    }
}

impl FromStr for Position {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts = s.split_whitespace().collect::<Vec<&str>>();
        if parts.len() != 3 {
            Err(ParseError::new(format!("expected 3 parts for a Position, got {}", parts.len())))
        } else {
            Ok(Position {
                at: Point::from(parts[0], parts[1])?,
                heading: parts[2].parse()?,
            })
        }
    }
}

impl Point {
    fn from(x: &str, y: &str) -> Result<Point, ParseError> {
        Ok(Point {
            x: x.parse()?,
            y: y.parse()?,
        })
    }
}

impl FromStr for CardinalPoint {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "N" => Ok(CardinalPoint::North),
            "E" => Ok(CardinalPoint::East),
            "W" => Ok(CardinalPoint::West),
            "S" => Ok(CardinalPoint::South),
            _ => Err(ParseError::new(format!("invalid Cardinal Point: {}", s)))
        }
    }
}

impl FromStr for Instruction {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "A" => Ok(Instruction::MoveOnce),
            "G" => Ok(Instruction::TurnLeft),
            "D" => Ok(Instruction::TurnRight),
            _ => Err(ParseError::new(format!("invalid Instruction: {}", s)))
        }
    }
}

#[cfg(test)]
mod test {
    use std::io;

    use fluent_asserter::prelude::*;
    use rstest::rstest;

    use crate::mower::Instruction::{MoveOnce, TurnLeft, TurnRight};
    use crate::position::CardinalPoint;

    use super::*;

    #[rstest]
    #[case(vec ! [], "empty")]
    #[case(vec ! ["X 2"], "invalid digit")]
    #[case(vec ! ["1 X"], "invalid digit")]
    #[case(vec ! ["1 2 X"], "parts")]
    #[case(vec ! ["5 5", "1 2 E"], "incomplete Mower")]
    #[case(vec ! ["5 5", "1 2 W", "XDG"], "Instruction")]
    #[case(vec ! ["5 5", "1 2 X", "ADG"], "Cardinal Point")]
    fn should_not_parse_invalid_configuration(#[case] input: Vec<&str>, #[case] pattern: &str) {
        let result = read_configuration(io::Cursor::new(input.join("\n")));

        assert_that!(result.is_err()).is_true();
        assert_that!(result.unwrap_err().message.matches(pattern).count()).is_greater_than_or_equal_to(1);
    }

    #[test]
    fn should_parse_valid_configuration() {
        let input = vec![
            "4 5",
            "1 2 S",
            "DAAG",
            "3 5 W",
            "GGAAD",
            "1 1 E",
            "GGG",
            "2 2 N",
            "DDD",
        ].join("\n");
        let result = read_configuration(io::Cursor::new(input));

        assert_that!(result.is_ok()).is_true();

        assert_that!(result.unwrap()).is_equal_to(Configuration {
            top_right_corner: Point { x: 4, y: 5 },
            mowers: vec![
                MowerConfiguration {
                    start_position: Position {
                        at: Point { x: 1, y: 2 },
                        heading: CardinalPoint::South,
                    },
                    instructions: vec![TurnRight, MoveOnce, MoveOnce, TurnLeft],
                },
                MowerConfiguration {
                    start_position: Position {
                        at: Point { x: 3, y: 5 },
                        heading: CardinalPoint::West,
                    },
                    instructions: vec![TurnLeft, TurnLeft, MoveOnce, MoveOnce, TurnRight],
                },
                MowerConfiguration {
                    start_position: Position {
                        at: Point { x: 1, y: 1 },
                        heading: CardinalPoint::East,
                    },
                    instructions: vec![TurnLeft, TurnLeft, TurnLeft],
                },
                MowerConfiguration {
                    start_position: Position {
                        at: Point { x: 2, y: 2 },
                        heading: CardinalPoint::North,
                    },
                    instructions: vec![TurnRight, TurnRight, TurnRight],
                },
            ],
        });
    }
}